This is a MEAN stack sandbox app based off of the tutorial provided by Christopher Buecheler (links below). 

There are two parts, the basic website using NodeJS, Express, Jade and MongoDB. 
The second part is a single-page, ajax-driven data manipulation in Node, Express, MongoDB

Tutorials:
http://cwbuecheler.com/web/tutorials/2013/node-express-mongo/
http://cwbuecheler.com/web/tutorials/2014/restful-web-app-node-express-mongodb/


### To Do ###
* Add .gitignore for Node to keep library function out of source code
* create a update to the userlist for a complete REST CRUD application